﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinder : MonoBehaviour {

    [SerializeField] Waypoint start, end;

    Dictionary<Vector2Int, Waypoint> grid = new Dictionary<Vector2Int, Waypoint>();
    Queue<Waypoint> queue = new Queue<Waypoint>();
    List<Waypoint> path = new List<Waypoint>();
    Vector2Int[] directions = {
        Vector2Int.up,
        Vector2Int.right,
        Vector2Int.down,
        Vector2Int.left
    };
    Waypoint searchCenter;
    bool isRunning = true;

    public List<Waypoint> GetPath() {
        LoadBlocks();
        ColorStartAndEnd();
        BreadthFirstSearch();
        FormPath();
        return path;
    }

    private void LoadBlocks() {
        Waypoint[] waypoints = FindObjectsOfType<Waypoint>();
        foreach (Waypoint waypoint in waypoints) {
            Vector2Int pos = waypoint.GetGridPos();
            if (grid.ContainsKey(pos)) {
                Debug.LogWarning("Skipping overlapping block: " + waypoint);
            } else {
                grid.Add(pos, waypoint);
            }
        }
        print("Loaded " + grid.Count + " blocks.");
    }

    private void ColorStartAndEnd() {
        start.SetTopColor(Color.green);
        end.SetTopColor(Color.red);
    }

    private void BreadthFirstSearch() {
        queue.Enqueue(start);

        while (queue.Count > 0 && isRunning) {
            searchCenter = queue.Dequeue();
            HaltIfEndFound();
            ExploreNeighbors();
            searchCenter.isExplored = true;
        }
    }

    private void HaltIfEndFound() {
        if (searchCenter == end) {
            isRunning = false;
        }
    }

    private void ExploreNeighbors() {
        if (!isRunning) { return; }

        foreach (Vector2Int direction in directions) {
            Vector2Int neighborPos = searchCenter.GetGridPos() + direction;
            if (grid.ContainsKey(neighborPos)) {
                QueueNewNeighbor(neighborPos);
            }
        }
    }

    private void QueueNewNeighbor(Vector2Int neighborPos) {
        Waypoint neighbor = grid[neighborPos]; ;
        if (!neighbor.isExplored && !queue.Contains(neighbor)) {
            neighbor.exploredFrom = searchCenter;
            queue.Enqueue(neighbor);
        }
    }

    private void FormPath() {
        while (searchCenter != start) {
            path.Add(searchCenter);
            searchCenter = searchCenter.exploredFrom;
        }
        path.Add(start);
        path.Reverse();
    }
}
