﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[SelectionBase]
public class CubeEditor : MonoBehaviour {

    Waypoint waypoint;

    void Awake() {
        waypoint = GetComponent<Waypoint>();
    }

    // Update is called once per frame
    void Update () {
        int gridSize = waypoint.GetGridSize();
        Vector2 gridPos = waypoint.GetGridPos();

        SnapToGrid(gridSize, gridPos);
        UpdateLabel(gridSize, gridPos);
    }

    private void SnapToGrid(int gridSize, Vector2 gridPos) {
        transform.position = new Vector3(
            gridPos.x * gridSize,
            0,
            gridPos.y * gridSize
        );
    }

    private void UpdateLabel(int gridSize, Vector2 gridPos) {
        TextMesh label = gameObject.GetComponentInChildren<TextMesh>();
        string labelText = gridPos.x + "," + gridPos.y;

        label.text = labelText;
        gameObject.name = "Cube " + labelText;
    }
}
